;;; helm-rake.el --- A Helm interface for Rake -*- lexical-binding: t -*-

;; Copyright (C) 2012-2021 Victor Deryagin

;; Author: Victor Deryagin <vderyagin@gmail.com>
;; Maintainer: Victor Deryagin <vderyagin@gmail.com>
;; Created: 16 Sep 2012
;; Version: 0.2.1
;; Package-Requires: ((helm))

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(require 'ansi-color)
(require 'cl-lib)
(require 'compile)
(require 'eieio)
(require 'helm)
(require 'helm-utils)
(require 'seq)
(require 'subr-x)

(defgroup helm-rake nil
  "Helm interface for Rake"
  :prefix "helm-rake-"
  :group 'helm
  :group 'tools)

(defvar helm-rake-cache (make-hash-table :test #'equal)
  "Hash table used for caching Rake tasks.")

(defvar helm-rake-show-all-tasks nil
  "Enable/disable showing undocumented Rake tasks.")

(defcustom helm-rake-use-cache t
  "Cache lists of tasks for faster invocation."
  :group 'helm-rake
  :type '(choice (const :tag "Use cache" t)
                 (const :tag "Don't use cache" nil)))

(defcustom helm-rake-use-bundler nil
  "Use bundler for task execution."
  :group 'helm-rake
  :type '(choice (const :tag "Use bundler" t)
                 (const :tag "Don't use bundler" nil)))

(defvar helm-rake-task-location-regex
  (rx string-start "rake "
      (group (+ (not (any space "[]")))) ; task name
      (? "[" (+ (not (any "[]"))) "]")   ; args
      (+ (char space))
      (group (+ not-newline))            ; file
      ":"
      (group (+ digit))                  ; line number
      ":in ")
  "Regular expression for matching 'helm-rake --where TASK-NAME' output.
Match is performed on every line, contains three groups: task
name, file name, and line number.")

(defvar helm-rake-task-description-regex
  (rx string-start "rake "
      (group (+ (not (any space "[]")))) ; task name
      (? "[" (+ (not (any "[]"))) "]")   ; args
      (+ (any space)) "# "
      (or
       (+ not-newline)                   ; task description
       string-end                        ; no description
       ))
  "Regular expression for matching 'helm-rake --tasks' output.
Match is performed on every line, contains a single group - task
name.")

(define-compilation-mode rake-mode "Rake"
  "Mode for running rake tasks."
  (set (make-local-variable 'compilation-scroll-output) t)
  (add-hook 'compilation-filter-hook
            (lambda ()
              (let ((inhibit-read-only t))
                (ansi-color-apply-on-region (point-min) (point-max))))
            nil
            'make-it-local))

(defclass helm-rake-task ()
  ((name :type string
         :documentation "Task name")
   (desc :type string
         :documentation "Task name + description (as return by rake --tasks)")
   (file :type string
         :documentation "Path to file task is located in")
   (line :type integer
         :documentation "Line number at which task is defined"))
  :documentation "Class for representing a Rake task.")

(defun helm-rake-make-task (raw-description)
  "Create an instance of class `helm-rake-task' based on RAW-DESCRIPTION."
  (string-match helm-rake-task-description-regex raw-description)
  (let ((task (make-instance 'helm-rake-task)))
    (oset task name
          (match-string 1 raw-description))
    (oset task desc
          (string-trim
           (string-remove-suffix
            " # "
            (string-remove-prefix "rake " raw-description))))
    task))

(cl-defmethod helm-rake-task-documented-p ((task helm-rake-task))
  "Figure out whether given Rake task instance is documented or not."
  (string-match-p " # " (oref task desc)))

(cl-defmethod helm-rake-task-set-location ((task helm-rake-task) raw-location)
  "Discover given Rake task location (file name and line number).
Set appropriate fields in the instance."
  (string-match helm-rake-task-location-regex raw-location)
  (oset task file (match-string 2 raw-location))
  (oset task line (string-to-number (match-string 3 raw-location))))

(cl-defmethod helm-rake-task-goto-definition ((task helm-rake-task))
  "Jump to definition of given Rake task."
  (find-file (oref task file))
  (helm-goto-line (oref task line)))

(defclass helm-rake-task-set ()
  ((tasks :documentation "List of tasks contained in given task set")
   (timestamp :type float
              :initform (float-time)
              :documentation "Time when task set was most recently updated")
   (relevant-files :type list
                   :documentation "List of wildcards for files that may contain tasks"))
  :abstract t
  :documentation "Abstract class for representing a set of Rake tasks.")

(defclass helm-rake-local-task-set (helm-rake-task-set)
  ((root :initarg :root
         :type string
         :documentation "Directory where Rakefile is located")
   (relevant-files :initform '("[Rr]akefile"
                               "[Rr]akefile.rb"
                               "Gemfile.lock"
                               "rakelib/*.rake"
                               "lib/tasks/*.rake")))
  :documentation "A class for representing a project-local Rake task set.")

(defclass helm-rake-global-task-set (helm-rake-task-set)
  ((relevant-files :initform '("~/.rake/*.rake")))
  :documentation "A class for representing a global Rake task set.")

(cl-defmethod helm-rake-task-set-last-modified ((repo helm-rake-task-set))
  "Figure out the time given Rake task set was likely to be last modified."
  (cl-loop with files = (cl-loop for pattern in (oref repo relevant-files)
                                 nconc (file-expand-wildcards pattern 'full-path))
           for file in files
           for mtime = (float-time (nth 5 (file-attributes file)))
           maximize mtime))

(cl-defmethod helm-rake-task-set-fresh-p ((repo helm-rake-task-set))
  "Figure out if Rake task set is likely to be modified since it was fetched."
  (let ((last-modified (helm-rake-task-set-last-modified repo)))
    (and (floatp last-modified)
         (> (oref repo timestamp)
            last-modified))))

(defun helm-rake-make-local-task-set ()
  "Try to construct an instance of class `helm-rake-local-task-set'.
Return nil if there are no local rake tasks to be found."
  (let ((rake-dir
         (locate-dominating-file
          default-directory
          (lambda (dir)
            (cl-loop for rakefile in '("rakefile" "Rakefile" "rakefile.rb" "Rakefile.rb")
                     thereis (file-regular-p (expand-file-name rakefile dir))))))
        task-set)
    (when rake-dir
      (setq rake-dir (file-name-as-directory (expand-file-name rake-dir)))
      (setq task-set (gethash rake-dir helm-rake-cache))
      (unless (and helm-rake-use-cache
                   task-set
                   (helm-rake-task-set-fresh-p task-set))
        (setq task-set (make-instance 'helm-rake-local-task-set))
        (oset task-set root rake-dir))
      task-set)))

(defun helm-rake-make-global-task-set ()
  "Try to construct an instance of class `helm-rake-global-task-set'.
Return nil if there are no global rake tasks to be found."
  (let ((task-set (gethash nil helm-rake-cache)))
    (if (and helm-rake-use-cache
             task-set
             (helm-rake-task-set-fresh-p task-set))
        task-set
      (make-instance 'helm-rake-global-task-set))))

(cl-defgeneric helm-rake-task-set-default-dir nil
  "Get directory in context of which task of given set should be executed.")

(cl-defmethod helm-rake-task-set-default-dir ((repo helm-rake-local-task-set))
  (oref repo root))

(cl-defmethod helm-rake-task-set-default-dir ((_ helm-rake-global-task-set))
  (identity "~"))

(cl-defmethod helm-rake-task-set-project-name ((repo helm-rake-local-task-set))
  "Name of project local Rake task set belongs to (name of project root directory)."
  (let ((root (helm-rake-task-set-default-dir repo)))
    (file-name-base (directory-file-name root))))

(cl-defgeneric helm-rake-cmd-prefix nil
  "Starting elements of command line used to execute tasks from given set.")

(cl-defmethod helm-rake-cmd-prefix ((repo helm-rake-local-task-set))
  (let* ((binstub (expand-file-name "bin/rake" (helm-rake-task-set-default-dir repo)))
         (rake (if (file-exists-p binstub) binstub "rake")))
    (if (and helm-rake-use-bundler
             (not binstub)
             (executable-find "bundle")
             (locate-dominating-file default-directory "Gemfile"))
        '("bundle" "exec" "rake" "--no-system")
      (list rake "--no-system"))))

(cl-defmethod helm-rake-cmd-prefix ((_ helm-rake-global-task-set))
  '("rake" "--system"))

(cl-defgeneric helm-rake-task-set-hash-key nil
  "Key used to store tasks for given Rake task set in `helm-rake-cache' hash map.")

(cl-defmethod helm-rake-task-set-hash-key ((repo helm-rake-local-task-set))
  (oref repo root))

(cl-defmethod helm-rake-task-set-hash-key ((_ helm-rake-global-task-set))
  nil)

(cl-defmethod helm-rake-task-set-put-in-cache ((repo helm-rake-task-set))
  "Assuming caching is enabled, put tasks from current task set there."
  (when (and helm-rake-use-cache
             (oref repo tasks))
    (puthash (helm-rake-task-set-hash-key repo)
             repo
             helm-rake-cache)))

(cl-defmethod helm-rake-task-set-drop-from-cache ((repo helm-rake-task-set))
  "Drop cached tasks (if any) for given task set."
  (remhash (helm-rake-task-set-hash-key repo)
           helm-rake-cache))

(cl-defmethod helm-rake-task-set-short-description ((repo helm-rake-local-task-set))
  (thread-first repo
    helm-rake-task-set-default-dir
    file-name-as-directory
    abbreviate-file-name))

(cl-defmethod helm-rake-task-set-short-description ((_ helm-rake-global-task-set))
  (identity "global task set"))

(cl-defmethod helm-rake-task-set-get-tasks ((repo helm-rake-task-set))
  "Fetch a list of tasks for given task set, cache it if caching is enabled."
  (unless (slot-boundp repo 'tasks)
    (let* ((default-directory (file-name-as-directory
                               (helm-rake-task-set-default-dir
                                repo)))
           (repo-desc (helm-rake-task-set-short-description repo))
           (cmd (concat "RAKE_COLUMNS=666 "
                        (mapconcat #'identity (helm-rake-cmd-prefix repo) " ")
                        " --all --tasks --silent 2>/dev/null; sleep 0.1"))
           (process-name (format "rake tasks in %s" default-directory))
           (process-buffer (generate-new-buffer-name
                            (format " *helm-rake tasks in %s*" repo-desc)))
           (progress (make-progress-reporter
                      (format "Getting list of tasks in %s: " repo-desc)))
           (process (start-process-shell-command process-name
                                                 process-buffer
                                                 cmd)))
      (while (process-live-p process)
        (sleep-for 0.2)
        (progress-reporter-update progress))

      (progress-reporter-done progress)

      (unless (zerop (process-exit-status process))
        (error "Failed to get list of tasks in %s" default-directory))

      (oset repo timestamp (float-time))
      (oset repo tasks
            (seq-map
             #'helm-rake-make-task
             (seq-filter
              (apply-partially #'string-match-p helm-rake-task-description-regex)
              (split-string (with-current-buffer process-buffer (buffer-string))
                            "\n"
                            'skip-empty))))

      (kill-buffer process-buffer)
      (helm-rake-task-set-put-in-cache repo))))

(cl-defmethod helm-rake-task-set-get-locations ((repo helm-rake-task-set))
  "Populate location fields of tasks of given task set."
  (helm-rake-task-set-get-tasks repo)
  (unless (slot-boundp (car (oref repo tasks)) 'file)
    (let* ((default-directory (file-name-as-directory
                               (helm-rake-task-set-default-dir repo)))
           (cmd (concat (mapconcat #'identity (helm-rake-cmd-prefix repo) " ")
                        " --all --where --silent 2>/dev/null"))

           (process-name (format "rake tasks locations in %s" default-directory))
           (process-buffer (format " *helm-rake task locations in %s*" default-directory))
           (progress (make-progress-reporter
                      (format "Getting locations for tasks in %s: " default-directory)))
           (process (start-process-shell-command process-name
                                                 process-buffer
                                                 cmd)))

      (while (process-live-p process)
        (sleep-for 0.2)
        (progress-reporter-update progress))

      (progress-reporter-done progress)

      (unless (zerop (process-exit-status process))
        (error "Failed to get locations of tasks in %s" default-directory))

      (seq-each
       (lambda (task-location-line)
         (string-match helm-rake-task-location-regex task-location-line)
         (let* ((task-name (match-string 1 task-location-line))
                (task (object-assoc task-name 'name (oref repo tasks))))
           (helm-rake-task-set-location task task-location-line)))
       (seq-filter
        (apply-partially #'string-match-p helm-rake-task-location-regex)
        (split-string (with-current-buffer process-buffer (buffer-string))
                      "\n"
                      'skip-empty)))

      (kill-buffer process-buffer))))

(cl-defmethod helm-rake-task-set-default-task ((repo helm-rake-task-set))
  "Get a default Rake task for given task set (return `nil' if there's none)."
  (helm-rake-task-set-get-tasks repo)
  (object-assoc "default" 'name (oref repo tasks)))

(cl-defmethod helm-rake-run-tasks ((repo helm-rake-task-set) tasks)
  "Run chosen Rake tasks in *rake* buffer."
  (when (helm-rake-local-task-set-p repo)
    (cd (helm-rake-task-set-default-dir repo)))
  (let ((cmd (concat (mapconcat #'identity (helm-rake-cmd-prefix repo) " ")
                     " "
                     (mapconcat (lambda (task) (oref task name)) tasks " "))))
    (when helm-current-prefix-arg
      (setq cmd (read-string "Command to execute: " cmd)))
    (compilation-start cmd 'rake-mode)))

(cl-defmethod helm-rake-run-tasks-async ((repo helm-rake-task-set) tasks)
  "Run chosen Rake tasks in background."
  (when (helm-rake-local-task-set-p repo)
    (cd (helm-rake-task-set-default-dir repo)))
  (let ((cmd-list (append (helm-rake-cmd-prefix repo)
                          (mapcar (lambda (task) (oref task name)) tasks))))
    (when helm-current-prefix-arg
      (setq cmd-list
            (split-string
             (string-trim (read-string "Command to execute: "
                                       (mapconcat #'identity cmd-list " ")))
             " ")))
    (set-process-sentinel
     (apply #'start-process "rake task execution" nil cmd-list)
     (lambda (_ change) (message "Rake: %s" (string-trim change))))))

(defun helm-rake-reload-candidates ()
  "Drop cache of current helm source, forcing it to reload."
  (interactive)
  (let ((task-set (assoc-default 'task-set (helm-get-current-source))))
    (helm-rake-task-set-drop-from-cache task-set)
    (slot-makeunbound task-set 'tasks))
  (helm-force-update))

(defvar helm-rake-keymap
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map helm-map)
    (define-key map (kbd "M-.") (lambda () (interactive) (helm-select-nth-action 1)))
    (define-key map (kbd "C-<return>") (lambda () (interactive) (helm-select-nth-action 2)))
    (define-key map (kbd "M-d") #'helm-rake-run-default-task)
    (define-key map (kbd "M-r") #'helm-rake-reload-candidates)
    (define-key map (kbd "M-s") #'helm-rake-toggle-show-all-tasks)
    map)
  "Keymap used when selecting Rake tasks in `helm-rake'.")

(defclass helm-rake-source (helm-source)
  ((task-set :initarg :task-set)
   (keymap
    :initform
    'helm-rake-keymap)
   (candidates
    :initform
    (lambda ()
      (let ((set (assoc-default 'task-set (helm-get-current-source))))
        (when set
          (helm-rake-task-set-get-tasks set)
          (oref set tasks)))))
   (match-part
    :initform
    (lambda (str)
      (replace-regexp-in-string
       (rx (>= 2 (char space)) (+ not-newline))
       ""
       str)))
   (candidate-transformer
    :initform
    (lambda (candidates)
      (cl-loop for task in candidates
               for name = (oref task name)
               if (or helm-rake-show-all-tasks
                      (helm-rake-task-documented-p task))
               collect (cons (thread-last (oref task desc)
                               (replace-regexp-in-string (concat "\\`" name)
                                                         (propertize name 'face 'bold))
                               (replace-regexp-in-string "  # " "  "))
                             task))))
   (action-transformer
    :initform
    (lambda (actions _)
      (let ((set (assoc-default 'task-set (helm-get-current-source))))
        (seq-filter (lambda (action)
                      (or (helm-rake-local-task-set-p set)
                          (member (car action) '("Run" "Go to definition" "Run in background"))))
                    actions))))
   (mode-line
    :initform
    (lambda ()
      (let ((default-task (helm-rake-task-set-default-task (assoc-default 'task-set (helm-get-current-source)))))
        (list
         "task(s)"
         (mapconcat #'identity
                    (delq nil
                          (list "RET:Run"
                                "C-RET:Run in background"
                                (when default-task "M-d:Run default")
                                "M-.:Jump to definition"
                                "M-r:Reload"
                                (format "M-s:%s undocumented tasks"
                                        (if helm-rake-show-all-tasks "Hide" "Show"))))
                    " ")))))
   (action
    :initform
    '(("Run" . (lambda (_)
                 (helm-rake-run-tasks
                  (assoc-default 'task-set
                                 (helm-get-current-source))
                  (helm-marked-candidates))))
      ("Go to definition" . (lambda (task)
                              (helm-rake-task-set-get-locations
                               (assoc-default 'task-set (helm-get-current-source)))
                              (helm-rake-task-goto-definition task)))
      ("Run in background" . (lambda (_)
                               (helm-rake-run-tasks-async
                                (assoc-default 'task-set
                                               (helm-get-current-source))
                                (helm-marked-candidates))))
      ("Run with bundler" . (lambda (_)
                              (let ((helm-rake-use-bundler t))
                                (helm-rake-run-tasks
                                 (assoc-default 'task-set
                                                (helm-get-current-source))
                                 (helm-marked-candidates)))))
      ("Run without bundler" . (lambda (_)
                                 (let ((helm-rake-use-bundler nil))
                                   (helm-rake-run-tasks
                                    (assoc-default 'task-set
                                                   (helm-get-current-source))
                                    (helm-marked-candidates)))))))
   (persistent-action
    :initform
    (lambda (task)
      (helm-rake-task-set-get-locations
       (assoc-default 'task-set (helm-get-current-source)))
      (helm-rake-task-goto-definition task)))
   (persistent-help
    :initform "Show definition"))
  :documentation "A Helm source configured for Rake tasks")

(defun helm-rake-toggle-show-all-tasks ()
  "Toggle showing undocumented rake tasks for all helm sources."
  (interactive)
  (setq helm-rake-show-all-tasks (not helm-rake-show-all-tasks))
  (helm-force-update))

(defun helm-rake-run-default-task ()
  "Run default task for current Helm source, notify user if none found."
  (interactive)
  (let* ((task-set (assoc-default 'task-set (helm-get-current-source)))
         (default-task (helm-rake-task-set-default-task task-set)))
    (if default-task
        (helm-run-after-exit (lambda () (helm-rake-run-tasks task-set (list default-task))))
      (message "No default task"))))

;;;###autoload
(defun helm-rake ()
  "Preconfigured helm for Rake."
  (interactive)
  (let* ((helm-rake-show-all-tasks nil)
         (local-task-set (helm-rake-make-local-task-set))
         (global-task-set (helm-rake-make-global-task-set))
         (local-source (when local-task-set
                         (helm-rake-task-set-get-tasks local-task-set)
                         (and (oref local-task-set tasks)
                              (helm-make-source (format "Tasks for %s"
                                                        (helm-rake-task-set-project-name local-task-set))
                                  'helm-rake-source :task-set local-task-set))))
         (global-source (when global-task-set
                          (helm-rake-task-set-get-tasks global-task-set)
                          (and (oref global-task-set tasks)
                               (helm-make-source "Global tasks" 'helm-rake-source :task-set global-task-set))))
         (sources (or (delq nil (list local-source global-source))
                      (user-error "No rake tasks found"))))

    (helm :prompt "Task: "
          :truncate-lines t
          :buffer "*helm rake*"
          :sources sources)))

(provide 'helm-rake)

;;; helm-rake.el ends here
