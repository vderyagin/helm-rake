# helm-rake #

Helm-rake is an Emacs interface for [Rake][1] using [helm][2] for task selection.

## Installation & setup ##

```lisp
(define-key global-map (kbd "C-c r") #'helm-rake)
```

## Screenshot ##

![helm-rake screenshot](https://gitlab.com/vderyagin/helm-rake/raw/master/screenshot.gif)

## Functionality ##

Helm-rake supports running Rake tasks, quickly jumping to task definition.

### Default task ###

Press `M-d` to run default task (one that would be invoked if you simply run `rake` on command line).

### Project-local and system-wide tasks ###

Usually Rake is used within context of some project, with tasks defined in `Rakefile` or `rakelib/*.rake` files (in case of Rails, also in `lib/tasks/*.rake`). Helm-rake, of course, handles those. But Rake also supports system-wide tasks defined in `~/.rake/*.rake` which can be executed from anywhere in the system using `--system` Rake command line switch. If you have any of those, helm-rake will display them as separate Helm source.

### Running multiple tasks at once ###

Rake supports running multiple tasks in order (`rake task1 task2 … taskn`). Select multiple tasks (by default, using `C-SPC`), after you press `RET` tasks will be run in order they were selected in (as opposed to order they are listed in).

### Running tasks in background ###

By default tasks run in dedicated `*rake*` buffer so that you can see whatever output they produce. That is not always necessary, sometimes you don't care about output. To run task in background, press `C-RET` (works with multiple tasks too, see previous section). When execution finishes, Emacs will display appropriate message in minibuffer (telling if Rake finished execution successfully, with error, was killed or whatever).

### Jumping to task definition ###

Not much to say here, press `M-.` to go to task definition, `C-j` to do that without stopping helm-rake session.

### Editing command line before execution ###

Prefix argument (`C-u`) before kicking off task execution will allow you to first manually edit the command line.

### Undocumented tasks ###

By default only documented tasks are displayed (such that have `desc` corresponding to their `task` definition). To also show undocumented ones, press `M-s`. Note that usually undocumented tasks are intented to be invoked from code, not directly by user. This feature is probably more useful for "jump to definition" functionality rather then direct task execution.

### Execution via "bundler exec" ###

Sometimes there is a need to run given task using [Bundler][3] (like `bundler exec rake task_name`). You can either configure variable `helm-rake-use-bundler` to enable this globally (except system-wide tasks, for which it does not make sense) or you can just use extended actions (by pressing `TAB` and choosing appropriate option) to explicitly run given task with or without bundler.

## Performance ##

Rake is not very fast, especially with big tasks sets (listing all tasks on a newly generated Rails application takes ~0.8 seconds on my laptop), which helm-rake tries to mitigate by doing some caching. Cache for given project is flushed every time any of conventional files used for storing tasks is modified (`Rakefike`, files in `rakelib/` or `lib/tasks/`', files in `~/.rake/` for system-wide tasks) or when Gemfile.lock is modified (additional Rake tasks can be provided by external gems), so you should never get an inaccurate task listing. But in case you are doing something funny, like putting tasks in some unconventional location and then modifying them, pressing `M-r` forces reloading of taks list.

It is also possible to disable cache entirely by customizing `helm-rake-use-cache` variable.

Cache is not persisted between Emacs sessions.

## Issues ##

### Asynchronous processes ###

Emacs does not expect external processes it manages to launch asynchronous subprocesses; if they do, and subprocess wants to keep running after its parent has terminated, Emacs may kill it. Be aware of that if you are shelling out using `IO::popen`, `Kernel#spawn` or launching async subprocesses is some other way from your rake tasks and not waiting for them to exit.

Using ``Kernel#` ``, `Kernel#system`, as well as `FileUtils#sh` and `FileUtils#ruby` added by rake, is safe, because they work synchronously.

[1]: https://github.com/ruby/rake "A make-like build utility for Ruby"
[2]: https://emacs-helm.github.io/helm "Emacs incremental completion and selection narrowing framework"
[3]: http://bundler.io "Depencency manager for Ruby projects"
